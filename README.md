# Verkehrzeichen Stickerpack

## Setup

- Öffne irgendeinen Chat
- schreibe "/devtools" in den Chat
- drücke Enter

- klicke auf "Kontodaten erkunden"

![](assets/devtools.png)

- klicke auf "Sende benutzerdefiniertes Kontodatenereigniss"

![](assets/kontodatenereigniss.png)

- schreibe in das Eventtyp Eingabefeld (oben): `m.widgets`
- schreibe in das Ereignissinhalt Eingabefeld:

```json
{
    "stickerpicker": {
        "content": {
            "type": "m.stickerpicker",
            "url": "https://fl1tzi.codeberg.page/verkehrszeichen-stickerpicker/?theme=$theme",
            "name": "Stickerpicker",
            "data": {}
        },
        "sender": "@fl1tzi:matrix.fl1tzi.com",
        "state_key": "stickerpicker",
        "type": "m.widget",
        "id": "stickerpicker"
    }
}
```

- klicke auf Senden (unten rechts)
- schließe die Bestätigung

---

![](assets/ergebniss.png)
